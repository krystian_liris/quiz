#include "stdafx.h"
#include "Cpytania.h"
#include<time.h>
#include<cstdlib>

void Cpytania::init(){
	zdjeciaSciezka=gcnew System::Collections::Generic::List<System::String ^>;
	odpowiedzA=gcnew System::Collections::Generic::List<System::String ^>;
	odpowiedzB=gcnew System::Collections::Generic::List<System::String ^>;
	odpowiedzC=gcnew System::Collections::Generic::List<System::String ^>;
	odpowiedzD=gcnew System::Collections::Generic::List<System::String ^>;
	poprawnaOdpowiedz=gcnew System::Collections::Generic::List<System::String ^>;
	pytaniaKtorePadly=gcnew System::Collections::Generic::List<System::Int16> ;
	iloscPytan=0;
	System::IO::StreamReader ^pytaniaPlik;
	pytaniaPlik=gcnew System::IO::StreamReader("pytania.txt");
	System::String ^linia;

	while((linia=pytaniaPlik->ReadLine())!=nullptr){
		zdjeciaSciezka->Add(linia);
		odpowiedzA->Add(pytaniaPlik->ReadLine());
		odpowiedzB->Add(pytaniaPlik->ReadLine());
		odpowiedzC->Add(pytaniaPlik->ReadLine());
		odpowiedzD->Add(pytaniaPlik->ReadLine());
		poprawnaOdpowiedz->Add(pytaniaPlik->ReadLine());
		iloscPytan++;
	}
	pytaniaPlik->Close();
}

void Cpytania::losuj(void){
	if(pytaniaKtorePadly->Count!=iloscPytan){
		do{
			wylosowanePytanie=rand()%iloscPytan;
			for(int i=0;i<pytaniaKtorePadly->Count && wylosowanePytanie!=-1;i++){
				if(wylosowanePytanie==pytaniaKtorePadly[i])
					wylosowanePytanie=-1;
			}
		}while(wylosowanePytanie==-1);
	
		pytaniaKtorePadly->Add(wylosowanePytanie);
	}
}

Cpytania::Cpytania(void)
{
	
	srand(time(NULL));
	
	init();
}
System::String ^Cpytania::getZdjeciaSciezka(void){
	return zdjeciaSciezka[wylosowanePytanie];
}
System::String ^Cpytania::getOdpowiedz(int number){
	switch(number){
	case 0:
		return odpowiedzA[wylosowanePytanie];
		break;
	case 1:
		return odpowiedzB[wylosowanePytanie];
		break;
	case 2:
		return odpowiedzC[wylosowanePytanie];
		break;
	case 3:
		return odpowiedzD[wylosowanePytanie];
		break;
	}
}
System::String ^Cpytania::getPoprawnaOdpowiedz(void){
	return poprawnaOdpowiedz[wylosowanePytanie];
}
System::Int16 Cpytania::getIloscPytan(void){
	return iloscPytan;
}