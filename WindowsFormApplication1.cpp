// WindowsFormApplication1.cpp : main project file.

#include "stdafx.h"
#include"GlowneOkno.h"

using namespace WindowsFormApplication1;

[STAThreadAttribute]
int main(array<System::String ^> ^args)
{
	// Enabling Windows XP visual effects before any controls are created
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false); 

	// Create the main window and run it
	QuizKrystian::GlowneOkno ^glowne=gcnew QuizKrystian::GlowneOkno();
	Application::Run(glowne);
	return 0;
}
