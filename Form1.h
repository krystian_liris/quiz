#pragma once
#include<cstdlib>
#include<cmath>
#include<list>
#include"Cpytania.h"
#include<ctime>

namespace WindowsFormApplication1 {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for Form1
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{

	//private: System::Windows::Forms::TextBox^  columnName;






	public:
		Form1(void)
		{
			pytania=gcnew Cpytania;
			
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  GraNowaGraButton;
	protected: 

	protected: 
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  GraPoprawneLabel;
	private: System::Windows::Forms::GroupBox^  groupBox1;
	private: System::Windows::Forms::Label^  GraIloscPozostalychLabel;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  GraCzasLabel;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::GroupBox^  groupBox2;
	private: System::Windows::Forms::RadioButton^  OdpowiedziD;
	private: System::Windows::Forms::RadioButton^  OdpowiedziC;
	private: System::Windows::Forms::RadioButton^  OdpowiedziB;
	private: System::Windows::Forms::RadioButton^  OdpowiedziA;
	private: System::Windows::Forms::Button^  SprawdzOdpowiedzButton;
	private: Cpytania ^pytania;
	private: clock_t timer;



	private: System::Windows::Forms::MenuStrip^  menuStrip1;


	private: System::Windows::Forms::ToolStripMenuItem^  informacjeOAutorzeToolStripMenuItem;
	private: System::Windows::Forms::PictureBox^  ZdjeciePictureBox;
	private: System::ComponentModel::IContainer^  components;
	
	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->GraNowaGraButton = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->GraPoprawneLabel = (gcnew System::Windows::Forms::Label());
			this->groupBox1 = (gcnew System::Windows::Forms::GroupBox());
			this->GraIloscPozostalychLabel = (gcnew System::Windows::Forms::Label());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->GraCzasLabel = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->groupBox2 = (gcnew System::Windows::Forms::GroupBox());
			this->OdpowiedziD = (gcnew System::Windows::Forms::RadioButton());
			this->OdpowiedziC = (gcnew System::Windows::Forms::RadioButton());
			this->OdpowiedziB = (gcnew System::Windows::Forms::RadioButton());
			this->OdpowiedziA = (gcnew System::Windows::Forms::RadioButton());
			this->SprawdzOdpowiedzButton = (gcnew System::Windows::Forms::Button());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->informacjeOAutorzeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->ZdjeciePictureBox = (gcnew System::Windows::Forms::PictureBox());
			this->groupBox1->SuspendLayout();
			this->groupBox2->SuspendLayout();
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ZdjeciePictureBox))->BeginInit();
			this->SuspendLayout();
			// 
			// GraNowaGraButton
			// 
			this->GraNowaGraButton->Location = System::Drawing::Point(23, 143);
			this->GraNowaGraButton->Name = L"GraNowaGraButton";
			this->GraNowaGraButton->Size = System::Drawing::Size(142, 41);
			this->GraNowaGraButton->TabIndex = 0;
			this->GraNowaGraButton->Text = L"Nowa gra";
			this->GraNowaGraButton->UseVisualStyleBackColor = true;
			this->GraNowaGraButton->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// label1
			// 
			this->label1->Location = System::Drawing::Point(8, 52);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(171, 21);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Ilosc poprawnych odpowiedzi:";
			this->label1->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// GraPoprawneLabel
			// 
			this->GraPoprawneLabel->Location = System::Drawing::Point(8, 73);
			this->GraPoprawneLabel->Name = L"GraPoprawneLabel";
			this->GraPoprawneLabel->Size = System::Drawing::Size(171, 24);
			this->GraPoprawneLabel->TabIndex = 2;
			this->GraPoprawneLabel->Text = L"0";
			this->GraPoprawneLabel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// groupBox1
			// 
			this->groupBox1->Controls->Add(this->GraIloscPozostalychLabel);
			this->groupBox1->Controls->Add(this->label3);
			this->groupBox1->Controls->Add(this->GraCzasLabel);
			this->groupBox1->Controls->Add(this->label2);
			this->groupBox1->Controls->Add(this->GraPoprawneLabel);
			this->groupBox1->Controls->Add(this->label1);
			this->groupBox1->Controls->Add(this->GraNowaGraButton);
			this->groupBox1->Location = System::Drawing::Point(401, 27);
			this->groupBox1->Name = L"groupBox1";
			this->groupBox1->Size = System::Drawing::Size(185, 190);
			this->groupBox1->TabIndex = 3;
			this->groupBox1->TabStop = false;
			this->groupBox1->Text = L"Gra";
			// 
			// GraIloscPozostalychLabel
			// 
			this->GraIloscPozostalychLabel->Location = System::Drawing::Point(8, 29);
			this->GraIloscPozostalychLabel->Name = L"GraIloscPozostalychLabel";
			this->GraIloscPozostalychLabel->Size = System::Drawing::Size(171, 23);
			this->GraIloscPozostalychLabel->TabIndex = 6;
			this->GraIloscPozostalychLabel->Text = L"0";
			this->GraIloscPozostalychLabel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(33, 16);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(120, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"Ilosc pozostalych pytan:";
			this->label3->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// GraCzasLabel
			// 
			this->GraCzasLabel->Location = System::Drawing::Point(11, 110);
			this->GraCzasLabel->Name = L"GraCzasLabel";
			this->GraCzasLabel->Size = System::Drawing::Size(168, 23);
			this->GraCzasLabel->TabIndex = 4;
			this->GraCzasLabel->Text = L"0";
			this->GraCzasLabel->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(68, 97);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(50, 13);
			this->label2->TabIndex = 3;
			this->label2->Text = L"Czas gry:";
			this->label2->TextAlign = System::Drawing::ContentAlignment::MiddleCenter;
			// 
			// groupBox2
			// 
			this->groupBox2->Controls->Add(this->OdpowiedziD);
			this->groupBox2->Controls->Add(this->OdpowiedziC);
			this->groupBox2->Controls->Add(this->OdpowiedziB);
			this->groupBox2->Controls->Add(this->OdpowiedziA);
			this->groupBox2->Location = System::Drawing::Point(20, 223);
			this->groupBox2->Name = L"groupBox2";
			this->groupBox2->Size = System::Drawing::Size(363, 157);
			this->groupBox2->TabIndex = 4;
			this->groupBox2->TabStop = false;
			this->groupBox2->Text = L"Kogo przedstawia zdjecie powyzej\?";
			// 
			// OdpowiedziD
			// 
			this->OdpowiedziD->AutoSize = true;
			this->OdpowiedziD->Location = System::Drawing::Point(191, 96);
			this->OdpowiedziD->Name = L"OdpowiedziD";
			this->OdpowiedziD->Size = System::Drawing::Size(14, 13);
			this->OdpowiedziD->TabIndex = 5;
			this->OdpowiedziD->TabStop = true;
			this->OdpowiedziD->UseVisualStyleBackColor = true;
			// 
			// OdpowiedziC
			// 
			this->OdpowiedziC->AutoSize = true;
			this->OdpowiedziC->Location = System::Drawing::Point(21, 96);
			this->OdpowiedziC->Name = L"OdpowiedziC";
			this->OdpowiedziC->Size = System::Drawing::Size(14, 13);
			this->OdpowiedziC->TabIndex = 5;
			this->OdpowiedziC->TabStop = true;
			this->OdpowiedziC->UseVisualStyleBackColor = true;
			// 
			// OdpowiedziB
			// 
			this->OdpowiedziB->AutoSize = true;
			this->OdpowiedziB->Location = System::Drawing::Point(191, 32);
			this->OdpowiedziB->Name = L"OdpowiedziB";
			this->OdpowiedziB->Size = System::Drawing::Size(14, 13);
			this->OdpowiedziB->TabIndex = 5;
			this->OdpowiedziB->TabStop = true;
			this->OdpowiedziB->UseVisualStyleBackColor = true;
			// 
			// OdpowiedziA
			// 
			this->OdpowiedziA->AutoSize = true;
			this->OdpowiedziA->Location = System::Drawing::Point(21, 32);
			this->OdpowiedziA->Name = L"OdpowiedziA";
			this->OdpowiedziA->Size = System::Drawing::Size(14, 13);
			this->OdpowiedziA->TabIndex = 0;
			this->OdpowiedziA->TabStop = true;
			this->OdpowiedziA->UseVisualStyleBackColor = true;
			// 
			// SprawdzOdpowiedzButton
			// 
			this->SprawdzOdpowiedzButton->Location = System::Drawing::Point(401, 223);
			this->SprawdzOdpowiedzButton->Name = L"SprawdzOdpowiedzButton";
			this->SprawdzOdpowiedzButton->Size = System::Drawing::Size(185, 157);
			this->SprawdzOdpowiedzButton->TabIndex = 6;
			this->SprawdzOdpowiedzButton->Text = L"Sprawdz odpowiedz";
			this->SprawdzOdpowiedzButton->UseVisualStyleBackColor = true;
			this->SprawdzOdpowiedzButton->Click += gcnew System::EventHandler(this, &Form1::SprawdzOdpowiedzButton_Click);
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->informacjeOAutorzeToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(606, 24);
			this->menuStrip1->TabIndex = 7;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// informacjeOAutorzeToolStripMenuItem
			// 
			this->informacjeOAutorzeToolStripMenuItem->Name = L"informacjeOAutorzeToolStripMenuItem";
			this->informacjeOAutorzeToolStripMenuItem->Size = System::Drawing::Size(128, 20);
			this->informacjeOAutorzeToolStripMenuItem->Text = L"Informacje o autorze";
			this->informacjeOAutorzeToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::informacjeOAutorzeToolStripMenuItem_Click);
			// 
			// ZdjeciePictureBox
			// 
			this->ZdjeciePictureBox->Location = System::Drawing::Point(20, 27);
			this->ZdjeciePictureBox->Name = L"ZdjeciePictureBox";
			this->ZdjeciePictureBox->Size = System::Drawing::Size(362, 189);
			this->ZdjeciePictureBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->ZdjeciePictureBox->TabIndex = 8;
			this->ZdjeciePictureBox->TabStop = false;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(606, 389);
			this->Controls->Add(this->ZdjeciePictureBox);
			this->Controls->Add(this->SprawdzOdpowiedzButton);
			this->Controls->Add(this->groupBox2);
			this->Controls->Add(this->groupBox1);
			this->Controls->Add(this->menuStrip1);
			this->Cursor = System::Windows::Forms::Cursors::Arrow;
			this->KeyPreview = true;
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"Quiz";
			this->groupBox1->ResumeLayout(false);
			this->groupBox1->PerformLayout();
			this->groupBox2->ResumeLayout(false);
			this->groupBox2->PerformLayout();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->ZdjeciePictureBox))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

private: System::Void informacjeOAutorzeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 MessageBox::Show("Siemanko to ja Krystian Liris");
		 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			 pytania->init();
			GraIloscPozostalychLabel->Text=Convert::ToString(pytania->getIloscPytan());
			GraCzasLabel->Text="0";
			GraPoprawneLabel->Text="0";
			timer=clock();
			pytania->losuj();
			ZdjeciePictureBox->Load(pytania->getZdjeciaSciezka());
			OdpowiedziA->Text="A)"+pytania->getOdpowiedz(0);
			OdpowiedziB->Text="B)"+pytania->getOdpowiedz(1);
			OdpowiedziC->Text="C)"+pytania->getOdpowiedz(2);
			OdpowiedziD->Text="D)"+pytania->getOdpowiedz(3);
		 }
private: System::Void SprawdzOdpowiedzButton_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(GraIloscPozostalychLabel->Text!="0"){
				 GraCzasLabel->Text=Convert::ToString((clock()-timer)/CLOCKS_PER_SEC)+" s";
				 System::Int16 odpowiedz;
				 if(OdpowiedziA->Checked)
					 odpowiedz=0;
				 else if(OdpowiedziB->Checked)
					 odpowiedz=1;
				 else if(OdpowiedziC->Checked)
					 odpowiedz=2;
				 else if(OdpowiedziD->Checked)
					 odpowiedz=3;
				 else
					 odpowiedz=4;


				 if(odpowiedz==4)
					 MessageBox::Show("Nie zaznaczono odpowiedzi!");
				 else{
					 if(Convert::ToString(odpowiedz)==pytania->getPoprawnaOdpowiedz()){
						 GraPoprawneLabel->Text=Convert::ToString(Convert::ToInt16(GraPoprawneLabel->Text)+1);
						 MessageBox::Show("Poprawna odpowiedz!");
					 }
					 else{
						 MessageBox::Show("Bledna odpowiedz!");
					 }
					 GraIloscPozostalychLabel->Text=Convert::ToString(Convert::ToInt16(GraIloscPozostalychLabel->Text)-1);
					 if(GraIloscPozostalychLabel->Text=="0")
						 MessageBox::Show("Koniec gry! Udzielono "+GraPoprawneLabel->Text+" odpowiedzi na "+Convert::ToString(pytania->getIloscPytan())+" wszystkich pytan w czasie "+ GraCzasLabel->Text+". Gratulujemy!");
					 pytania->losuj();
					 ZdjeciePictureBox->Load(pytania->getZdjeciaSciezka());
					OdpowiedziA->Text="A)"+pytania->getOdpowiedz(0);
					OdpowiedziB->Text="B)"+pytania->getOdpowiedz(1);
					OdpowiedziC->Text="C)"+pytania->getOdpowiedz(2);
					OdpowiedziD->Text="D)"+pytania->getOdpowiedz(3);
				 }
			 }
			 else
				 MessageBox::Show("Aby rozpoczac nowa gre wcisnij przycisk nowa gra");
		 }
};
}

