#pragma once
#include"Form1.h"
namespace QuizKrystian {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for GlowneOkno
	/// </summary>
	public ref class GlowneOkno : public System::Windows::Forms::Form
	{
	public:
		GlowneOkno(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~GlowneOkno()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  OtworzQuizButton;
	protected: 

	protected: 

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->OtworzQuizButton = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// OtworzQuizButton
			// 
			this->OtworzQuizButton->Location = System::Drawing::Point(48, 67);
			this->OtworzQuizButton->Name = L"OtworzQuizButton";
			this->OtworzQuizButton->Size = System::Drawing::Size(184, 121);
			this->OtworzQuizButton->TabIndex = 0;
			this->OtworzQuizButton->Text = L"OtworzQuiz";
			this->OtworzQuizButton->UseVisualStyleBackColor = true;
			this->OtworzQuizButton->Click += gcnew System::EventHandler(this, &GlowneOkno::OtworzQuizButton_Click);
			// 
			// GlowneOkno
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 262);
			this->Controls->Add(this->OtworzQuizButton);
			this->Name = L"GlowneOkno";
			this->Text = L"GlowneOkno";
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void OtworzQuizButton_Click(System::Object^  sender, System::EventArgs^  e) {
				 WindowsFormApplication1::Form1 ^mainFrame=gcnew WindowsFormApplication1::Form1();
				 mainFrame->Show();
			 }
	};
}
